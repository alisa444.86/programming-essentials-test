/*<!-- Представьте себе ситуацию - вам нужно оценить, является ли комментарий пользователя на сайте
недопустимым с лексической точки зрения (то есть содержит слова, которые нельзя употреблят, например:
“огромные выигрыши в нашем казино набирай в браузере …. ”). Логично, что для этого нужно проверить
содержит ли комментарий пользователя на сайте слишком много спам-слов (например, 1-2 слова “бесплатно” -
 это ок, но 3 и больше уже явный признак спама ). Для этого вам нужно написать функцию (именно функцию) isSpam,
 которая будет принимать 3 аргумента:
- строку, которую ввел пользователь;
- слово, переизбыток которого считается спамом (например, “выигрыш”);
- количество повторений слова, которое считается спамом (например, 3 - то есть если слово “выигрыш”
встречается в строке 1-2 раза, это не спам, а если 3 и больше - спам).
И возвращать функция isSpam должна true, если в переданной строке спам-слово встречается указанное или
больше число раз, и false - если нет.
При этом и комментарий, и спм-слово, нужно ввести в поля ввода, которые написаны ниже (input) и повесить
выполнение функции на клик на кнопке “Проверить на спам”. Результат выполнения функции вывести
 в p с id spam-check-result.

-->*/

const commentInput = document.querySelector('#comment');
const spamWordInput = document.querySelector('#spam-word');
const btn = document.querySelector('#send-comment');
const result = document.querySelector('#spam-check-result');

btn.addEventListener('click', () => {
    let comment = commentInput.value;
    let spamWord = spamWordInput.value;
    isSpam(comment, spamWord, 2)
});


const isSpam = (comment, spamWord, repeatAmount) => {
    let counter = 0;
    const array = comment.split(' ');

    for (let i = 0; i < array.length; i++) {
        if (array[i] === spamWord) {
            counter++
        }
    }

    if (counter >= repeatAmount) {
        result.innerText = `Вы не прошли проверку на спам. Слово ${spamWord} встречается ${counter} раз`;
    } else {
        result.innerText = `Вы прошли проверку на спам`;
    }
};